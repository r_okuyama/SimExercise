#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <functional>
#include "Eigen/Core"
#include "Eigen/LU"

using namespace std;
using namespace Eigen;

//Eigen LU
inline void solve_linear_system(MatrixXd &A, VectorXd &u, VectorXd &b, int &Node)
{
  FullPivLU<MatrixXd> lu(A);
  u = lu.solve(b);
}

inline void setMatrix(MatrixXd &A, VectorXd &b, VectorXd &f, int &Ele, double &h,double &k,double &a)
{
  int i;
  double tau;
  double alpha;

  alpha = abs(a)*h/(2*k);
  tau = h/(2.0*abs(a))*(1.0/tanh(alpha)-1.0/alpha);
	
  for(i=0;i<Ele;i++)
    {
      A(i,i)+=-a/2.0+tau*a*a/h+k/h; A(i,i+1)+=a/2.0-tau*a*a/h-k/h;
      A(i+1,i)+=-a/2.0-tau*a*a/h-k/h; A(i+1,i+1)+=a/2.0+tau*a*a/h+k/h;
      
      b[i]+=f[i]*h/2.0-tau*a*f[i];
      b[i+1]=f[i+1]*h/2.0+tau*a*f[i+1];
    }
}

inline void boundary(int &bc,double &G0,double &G1,double &Q0,double &Q1, MatrixXd &A, VectorXd &b,int &Node)
{
  int i;
  if(bc==1)
    {
      for(i=0;i<Node;i++)
	{
	  A(0,i)=0.0;
	  A(Node-1,i)=0.0;
	} 
      A(0,0)=1.0; b[0]=G0;
      A(Node-1,Node-1)=1.0; b[Node-1]=G1;
    }
  if(bc==2)
    {
      for(i=0;i<Node;i++)
	{
	  A(Node-1,i)=0.0;
	} 
      A(Node-1,Node-1)=1.0;b[Node-1]=G1;
      b[0]+=-Q1;
    }
  if(bc==3)
    {
      for(i=0;i<Node;i++)
	{
	  A(0,i)=0.0;
	} 
      A(0,0)=1.0;b[0]=G0;
      b[Node-1]+=Q0;
    }
}

//exact solution
inline void exactSol(double &a, double &k, const int &L, const double &h, VectorXd &u, int &Node)
{
  int x;
  for(x=0; x<Node; x++)
    {
      u[x] = 1.0 - (exp(a*x*h/k)-1.0)/(exp(a*L/k)-1.0);
    }
}

int main()
{
  int i;
  int Ele=100; //要素数N
  int Node=Ele+1; //境界点N+1
  double L=1.0; //領域長さ
  double h=(double)L/(double)Ele; //要素長さ
  double k=0.001; //拡散係数
  double a=0.1; //移流速度
  int bc=1;//境界条件 bc=1 both Diriclet bc=2 left Neumann bc=3 right Neumann
  double G0=1.0; //ディレクレ境界条件g(0)
  double G1=0.0; //ディレクレ境界条件g(1)
  double Q0=0.0; //ノイマン境界条件q(0)
  double Q1=0.0; //ノイマン境界条件q(1)
  VectorXd u = VectorXd::Zero(Node); //理論解
  VectorXd u1 = VectorXd::Zero(Node);
  VectorXd u2 = VectorXd::Zero(Node);
  VectorXd u3 = VectorXd::Zero(Node);
  VectorXd f = VectorXd::Zero(Node);
  MatrixXd A = MatrixXd::Zero(Node,Node);
  VectorXd b = VectorXd::Zero(Node);

  ofstream fk;
  fk.open("result/output.txt");

  k = 0.001;
  exactSol(a,k,L,h,u,Node);
  
  k = 0.001;
  setMatrix(A,b,f,Ele,h,k,a);
  boundary(bc,G0,G1,Q0,Q1,A,b,Node);
  solve_linear_system(A,u1,b,Node);
    
  A = MatrixXd::Zero(Node,Node);
  b = VectorXd::Zero(Node);
    
  k = 0.01;
  setMatrix(A,b,f,Ele,h,k,a);
  boundary(bc,G0,G1,Q0,Q1,A,b,Node);
  solve_linear_system(A,u2,b,Node);
    
  A = MatrixXd::Zero(Node,Node);
  b = VectorXd::Zero(Node);
    
  k = 0.1;
  setMatrix(A,b,f,Ele,h,k,a);
  boundary(bc,G0,G1,Q0,Q1,A,b,Node);
  solve_linear_system(A,u3,b,Node);
  
  for(i=0;i<Node;i++) cout<<u[i]<<" "<<u1[i]<<" "<<u2[i]<<" "<<u3[i]<<endl;
  for(i=0;i<Node;i++) fk<<h*i<<" "<<u[i]<<" "<<u1[i]<<" "<<u2[i]<<" "<<u3[i]<<endl;

  return 0;
}
