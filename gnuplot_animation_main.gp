#-------------------------------------------------------------------------------
# gnuplotの設定
#-------------------------------------------------------------------------------
reset
set nokey                # 凡例の非表示
set xrange [0:1]      # x軸方向の範囲の設定
set yrange [0:1]      # y軸方向の範囲の設定
set size square          # 図を正方形にする

set output "output_main.gif" # 出力ファイル名の設定
filename = sprintf("result/output.txt") # データファイルの名前の生成
plot filename using 1:3 with lines, filename using 1:4 with lines, filename using 1:5 with lines

set output "output_main_c.gif" # 出力ファイル名の設定
filename = sprintf("result/output.txt") # データファイルの名前の生成
plot filename using 1:2 with lines, filename using 1:3 with lines
