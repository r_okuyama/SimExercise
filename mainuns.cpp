#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <string>
#include <ios>
#include <sstream>
#include "Eigen/Core"
#include "Eigen/LU"

#define PRINT_MAT(X) cout<<#X<<" :n "<< X <<endl<<endl

using namespace std;
using namespace Eigen;

//陰解法(Eigen LU)
inline void solve_linear_system(MatrixXd &A, VectorXd &u, VectorXd &b)
{
  FullPivLU<MatrixXd> lu(A);
  u = lu.solve(b);
}

//Crank-Nicolson
inline void CNsetMatrix(MatrixXd &A, MatrixXd &C, VectorXd &b, VectorXd &f, VectorXd &u, int &Ele, int &Node, double &h,double &k, double &a, double &dt)
{
  int i;
  float tau;
  float alpha;
  
  alpha = abs(a)*h/(2.0*k);
  tau = h/(2.0*abs(a))*(1.0/tanh(alpha)-1.0/alpha);
  
  A = MatrixXd::Zero(Node,Node);
  b = VectorXd::Zero(Node);
  C = MatrixXd::Zero(Node,Node);

  for(i=0;i<Ele;i++)
    {
      A(i,i)+=(h/6.0*2.0+a*tau/2.0)/dt+(-a/2.0+tau*a*a/h+k/h)/2.0;
      A(i,i+1)+=(h/6.0-a*tau/2.0)/dt+(a/2.0-tau*a*a/h-k/h)/2.0;
      A(i+1,i)+=(h/6.0-a*tau/2.0)/dt+(-a/2.0-tau*a*a/h-k/h)/2.0;
      A(i+1,i+1)+=(h/6.0*2.0+a*tau/2.0)/dt+(a/2.0+tau*a*a/h+k/h)/2.0;
      
      b[i]+=f[i]*h/2.0-tau*a*f[i];
      b[i+1]+=f[i+1]*h/2.0+tau*a*f[i+1];

      C(i,i)+=(h/6.0*2.0+a*tau/2.0)/dt-(-a/2.0+tau*a*a/h+k/h)/2.0;
      C(i,i+1)+=(h/6.0-a*tau/2.0)/dt-(a/2.0-tau*a*a/h-k/h)/2.0;
      C(i+1,i)+=(h/6.0-a*tau/2.0)/dt-(-a/2.0-tau*a*a/h-k/h)/2.0;
      C(i+1,i+1)+=(h/6.0*2.0+a*tau/2.0)/dt-(a/2.0+tau*a*a/h+k/h)/2.0;
    }
  b += C * u;
  
  //境界条件設定
  for(i=0;i<Node;i++)
    {
      A(0,i)=0.0;
      //A(Node-1,i)=0.0; //u[Ele]の境界条件設定
    } 
  A(0,0)=1.0; b[0]=1.0;
  //A(Node-1,Node-1)=1.0; b[Node-1]=0.0; //u[Ele]の境界条件設定
}

//陽解法
inline void EXsetMatrix(MatrixXd &A, VectorXd &b, VectorXd &f, VectorXd &uE, int &Ele, int &Node, double &h,double &k, double &a, double &dt)
{
  int i,j;
  float tau;
  float alpha;
  alpha = abs(a)*h/(2.0*k);
  tau = h/(2.0*abs(a))*(1.0/tanh(alpha)-1.0/alpha);

  MatrixXd luM = MatrixXd::Zero(Node,Node); //質量集中行列 lumped mass matrix
  MatrixXd luinvM = MatrixXd::Zero(Node,Node); //質量集中逆行列 inversed lumped mass matrix

  A = MatrixXd::Zero(Node,Node);
  b = VectorXd::Zero(Node);

  for(i=0;i<Ele;i++)
    {
      luM(i,i)+=h/2.0;
      luM(i+1,i+1)+=h/2.0;
      
      A(i,i)+=(h/6.0*2.0+a*tau/2.0)/dt-(-a/2.0+tau*a*a/h+k/h);
      A(i,i+1)+=(h/6.0-a*tau/2.0)/dt-(a/2.0-tau*a*a/h-k/h);
      A(i+1,i)+=(h/6.0-a*tau/2.0)/dt-(-a/2.0-tau*a*a/h-k/h);
      A(i+1,i+1)+=(h/6.0*2.0+a*tau/2.0)/dt-(a/2.0+tau*a*a/h+k/h);
      
      b[i]+=f[i]*h/2.0-tau*a*f[i];
      b[i+1]+=f[i+1]*h/2.0+tau*a*f[i+1];
    }
  
  for(i=0;i<Node;i++)
    {
      for(j=0;j<Node;j++)
	{
	  if(fabs(luM(i,j)-0.0)<1e-6)
	  //if(luM(i,j)==0.0)
	    {
	      luinvM(i,j) = 0.0;
	    }
	  else
	    {
	      luinvM(i,j) = 1.0 / luM(i,j);
	    }
	} 
    }
  //境界条件設定
  uE[0] = 1.0;
  uE = luinvM * dt * (b + A * uE);
}

//解析解
inline void exactSol(double &a, double &k, const double &h, VectorXd &uex, int &Node, double &t)
{
  int x;
  for(x=0; x<Node; x++)
    {
      uex[x] = 1.0/2.0*exp(a*x*h/(2.0*k))*
	((exp(-a*x*h/(2.0*k))*erfc((x*h-a*t)/(2.0*sqrt(k*t))))
	 +exp(a*x*h/(2.0*k))*erfc((x*h+a*t)/(2.0*sqrt(k*t))));  
    }
}

int main()
{
  int i;
  double T;
  double dt = 0.001;
  int Ele=100; //要素数N
  int Node=Ele+1; //境界点N+1
  double L=1.0; //領域長さ
  double h=L/(double)Ele; //要素長さ
  double k=0.01; //拡散係数
  double a=0.5; //移流速度
  VectorXd u = VectorXd::Zero(Node); //陰解法
  VectorXd uex = VectorXd::Zero(Node); //解析解
  VectorXd uE = VectorXd::Zero(Node); //陽解法
  VectorXd f = VectorXd::Zero(Node);
  MatrixXd A = MatrixXd::Zero(Node,Node);
  VectorXd b = VectorXd::Zero(Node);
  MatrixXd C = MatrixXd::Zero(Node,Node);

  for(i=0;i<6;i++){
    //初期条件設定
    //u[Ele/2+3-i] = -0.01*(i-3)*(i-3)+0.225;
  }
  
  FILE* fp;
  char filename[100];
  for(T=0.0;T<dt*2000.0;T+=dt){
    sprintf(filename, "result/output_%04d.txt",(int)(T/dt));
    fp = fopen(filename,"w");

    cout << "t = " << T << endl;
    
    CNsetMatrix(A,C,b,f,u,Ele,Node,h,k,a,dt);
    
    solve_linear_system(A,u,b);
    
    EXsetMatrix(A,b,f,uE,Ele,Node,h,k,a,dt);
    
    exactSol(a,k,h,uex,Node,T);
      
    for(i=0;i<Node;i++) cout<<uE[i]<<endl;
    for(i=0;i<Node;i++) fprintf(fp,"%f %f %f %f\n",h*i,u[i],uex[i],uE[i]);
    fclose(fp);
  }
  return 0;
}
